def retrieval(title):
    import pandas as pd
    import numpy as np
    from numpy import load

    movies = pd.read_csv('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\movie_descriptions.csv')
    data_dict = load('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\cos_matrix.npz')
    cos_matrix = data_dict['arr_0']

    idx = movies[movies['original_title'] == title].index.values[0]
    sim_scores = list(enumerate(cos_matrix[idx]))
    sim_scores = sorted(sim_scores, key = lambda x:x[1], reverse = True)

    sim_scores = sim_scores[1:31]
    indices = [i[0] for i in sim_scores]
    titles = movies.iloc[indices][['original_title','id']]

    # titles = movies.iloc[indices][['original_title']].values

    return titles



# import pickle

# ret = pickle.load(open("C:\\Users\\MatiasSanchezWilson\\storefront\\website\\retrieved_ids.p", "rb"))

# print(type(ret))
# print(ret['The Dark Knight'][:6])


# import pandas as pd

# df = pd.DataFrame(ret['The Dark Knight'], columns=['id'])
# print(df)



# # load pickle called ret
# ret = pickle.load(open("C:\\Users\\MatiasSanchezWilson\\storefront\\website\\retrieved_ids.p", "rb"))
# # get title values from ret (dictionary) and make that into dataframe
# df = pd.DataFrame(ret[title], columns=['id'])

# boom