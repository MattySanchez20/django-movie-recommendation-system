# Django Movie Recommendation System

## Task:

The stakeholder requested a recommendation system that returns top 5 films that the viewer is likely to enjoy based on their favourite film. The recommendation system needs to be deployed within a website/app.

## Django as a Framework

Django is a web framework for building web applications in the Python programming language. It supplies a high-level, reusable set of components for building web applications quickly and easily, allowing developers to focus on writing code that is specific to their application's requirements, hence why it was chosen. The front-facing page was implemented by directing the main page through a URL. The URL guides Django to an app called playground. The URL looks like this 127.0.0.1/playground. The URL calls the playground app to run python functions stored in a file called views.py. A view is a field within every app and has custom made functions to add functionality to the website. For instance, the front-facing page has a URL of 127.0.0.1/playground/submit_film: the submit_film portion is a URL within playground that activates a function inside of the playground views.py file. In this case, the function is the recommendation system. 

## Machine Learning Model:

The recommendation system can broadly be split into two stages. The first stage is the retrieval, where the cosine similarity of the vectorised descriptions is computed. The movies are then ranked in order of cosine similarity and the top 30 most similar (highest cosine similarity) movies are returned. However, the performance of the recommendation can be improved further by implementing machine learned ranking on the retrieved results.  

Machine learned ranking refers to the process of ranking documents by order of relevance with respect to a query. In the case of this movie recommendation system the query is the movie title that the user wants to get recommendations from, the documents are the other movies that are to be recommended, and the true score for the relevance is the review score of that movie (used for training the movie). There are three main approaches to the machine learned ranking problem: Pointwise, where the relevance score for each document is evaluated. Pairwise, where the relevance of document pairs is evaluated and scored based on which document is more relevant. And Listwise, where the machine learning model is optimised by using one of the evaluation metrics for the ranked list. In the case of this recommendation system, a pairwise approach was adopted. 

## Live Demonstration and Fuzzy Match

Watch the video below to see a live demonstration!

[![Watch the video](https://static.vecteezy.com/system/resources/thumbnails/007/126/491/small/music-play-button-icon-vector.jpg)](Movie_Rec_and_6_more_pages_-_Work_-_Microsoft__Edge_2023-02-06_10-22-57.mp4)

As you can see from the video, I did not exactly type the name of the film. This was made possible by implementing fuzzy match which compares 2 strings. The 2 strings being compared would be the user input and the name of the title which is returned in a loop iterating through all of the film titles in the dataset. The one with the highest similarity score is entered into the machine learning model.

---

## Contributors:

- James Bright
- Aaron Padam
- Matias Sanchez

## Personal Contributions:

- created the website using django framework
- implemented the machine learning model into the django website
- implemented the fuzzy match functionality to ensure user input flexibility
