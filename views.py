from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist

from store.models import Product
from website.recommend import recommend, description
from website.flexible import flexible_input


def say_hello(request):
    query_set = Product.objects.all() # this only makes the query, it does not run it and return a list of items. it waits it be evaluated i.e, it is lazy
    # .objects acts as a manager which interfaces with the database. it has a bunch of methods to query the database
    

    product = Product.objects.filter(pk=0).first() # this is an alternative to the try and except block below. Filter is a query set but when we use first() it returns first value from query
    exists = Product.objects.filter(pk=0).exists() # returns boolean value if the product does exist

    # try:
    #     product = Product.objects.get(pk=0) # pk is primary key we are looking for id = 1, we get an exception if id does not exist
    # except ObjectDoesNotExist:
    #     pass
    # count_set = Product.objects.count()

    # query_set.filter().filter().order_by() # a new query set

    # query_set[0:5] # this will evaluate the query

    # for product in query_set: # this will also evaluate the query
    #     print(product)

    return render(request, 'hello.html', {'name': 'Mosh'})


def test_view(request):

    return render(request, 'base.html', {'name': 'Matias'})


def submit_film(request):

    li =[['The Shadow Strikes'], ['Kandidaten'], ['The Gorilla'], ['Ricochet'], ["Weekend at Bernie's"]]

    if request.method == 'POST':

            favourite_film = request.POST['film']

            fuzzy_input = flexible_input(favourite_film)

            suggestions = recommend(fuzzy_input, 6)

            suggestion = suggestions.values
            # retrieve the names of the films and their descriptions

            desc = description(suggestions)

            return render(request, 'submit_film.html', {
                'fuzzy': f'{fuzzy_input}',
                'fav_film': f'{suggestion[0]}',
                'fav_film1': f'{suggestion[1]}',
                'fav_film2': f'{suggestion[2]}',
                'fav_film3': f'{suggestion[3]}',
                'fav_film4': f'{suggestion[4]}',
                'fav_film5': f'{suggestion[5]}',
                'desc1': desc[0],
                'desc2': desc[1],
                'desc3': desc[2],
                'desc4': desc[3],
                'desc5': desc[4],
                'desc6': desc[5],
                })
            # return redirect('success_page')
    else:
        return render(request, 'base.html')