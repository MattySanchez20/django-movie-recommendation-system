def recommend(title, n_films = 5):
    from website.retrieval import retrieval
    import numpy as np
    import pandas as pd
    from xgboost import XGBRanker
    import pickle


    # load pickle called ret
    ret = pickle.load(open("C:\\Users\\MatiasSanchezWilson\\storefront\\website\\retrieved_ids.p", "rb"))
    # get title values from ret (dictionary) and make that into dataframe
    ret = pd.DataFrame(ret[title], columns=['id'])

    df = pd.read_csv('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\movie_descriptions.csv')

    ret = pd.merge(ret, df, on='id')
    ret.drop(columns = 'Unnamed: 0', inplace = True)


    # ret = pickle.load(open("C:\\Users\\MatiasSanchezWilson\\storefront\\website\\retrieved_ids.p", "rb")) # changed for this
    # ret = retrieval(title)
    movie_features = pd.read_csv('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\movie_features.csv')
    movie_features.drop(columns = 'Unnamed: 0', inplace = True)
    model = XGBRanker()
    model.load_model('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\ranking_model_sklearn.txt')

    test_feature_lst = []
    for i in ret['id'].to_numpy():
        test_feature_lst.append(movie_features[movie_features['id'] == i].values.flatten())

    rec_features = pd.DataFrame(data = test_feature_lst, columns = movie_features.columns)
    rec_features['userId'] = 1
    rec_features = rec_features.set_index(['userId', 'id'])

    pred = model.predict(rec_features)

    ret['relevance'] = pred

    return ret.sort_values(by = 'relevance', ascending = False).head(n_films)['original_title']


def description(recom):

    import pandas as pd

    df_movie_desc = pd.read_csv('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\movie_descriptions.csv')

    descriptions = []

    for movie in recom.values:

        desc = df_movie_desc.loc[df_movie_desc['original_title'] == movie, ['description']].values.tolist()
        
        try:
            item = desc[0][0]
            
            word_count = len(item.split(' '))
            if word_count < 60:
                descriptions.append(item)
            else:
                descriptions.append(f'{item[:240]}...')
        except:
            descriptions.append("No description available")
    
    return descriptions
