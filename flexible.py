def flexible_input(search_string):
    
    # imports for the function to work
    from fuzzy_match import algorithims
    import pandas as pd

    # importing dataframe with movie descriptions
    df = pd.read_csv('C:\\Users\\MatiasSanchezWilson\\storefront\\website\\movie_descriptions.csv')
    
    # setting threshold for the best match to 0
    highest = 0

    # looping through all movie titles
    for title in df['original_title'].values.tolist():
        
        # cosine similarity returns value between 0 and 1, 1 being the same and 0 being nothing alike
        cos = algorithims.trigram(search_string, title)
        
        # conditional statement to set variable with the highest value (most similar title)
        if cos > highest:
            highest = cos
            max_title = title
    return max_title